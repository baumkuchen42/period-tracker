# Fertiliy prediction

- [Sequential predictions of menstrual cycle lengths by Paola Bortot, Guido Masarotto, Bruno Scarpa](https://academic.oup.com/biostatistics/article/11/4/741/371616)

    > “three over six rule” (Marshall, 1979), a conventional marker of the day of ovulation for each cycle was determined manually as the first time in the cycle that the minimum basal temperature over 3 consecutive days was above the maximum temperature over the 6 immediately preceding days (more about that [here](https://rivista-statistica.unibo.it/article/view/952))

## Two main methods of Fertiliy Awareness

- source: [Hanna Witton - I Tried 5 Different Fertility/Menstrual Tracking Apps](https://invidious.kavin.rocks/watch?v=MZquDVwR78E)

- symptothermal method, which is taking into account temp, discharge, cervix inspection, calendar
- billings method, only focussing on cervical mucus and discharge, harder, if you're not used to monitoring your discharge. just as reliable though

Further info from that video:

- discharge: fertility period starts when you start seeing it, ovulation is at its peak amount (you only know it's a peak afterwards ofc)
- temperature: take at the same time in the morning, minimum of 3h consecutive sleep before. you can manually adjust the temp if the time varies for about an hour from the avg time. for 30min too late, decr by 0.05°C -> time of temp taking should be given and compared to avg time (actually, research when the ideal time is)
- if temp is still high after the predicted ovulation, the prediction should be moved forward bc that means that it hasn't happened yet (in general predictions should be adjusted during the cycle)
- idea from Natural Cycles: ![image](/uploads/0733a03d96460b73e1c7adb63d7be335/image.png)
- "It's not about using your current cycle to predict future ones, it's about using your current cycle to predict your current cycle."
- use ovulation pain (first find out, what exactly it means)
- being able to adjust which symptoms are going to be tracked and in which order they appear is a very nice feature
- only input period as happening when confirmed by user
- feature to create custom tracking option (example: Read your Body)
- nice stats (Clue): ![image](/uploads/d46dd5c84b08d310bdff0bc804497bc9/image.png)

# Cycle length prediction

- [PREDICTING LENGTH OF MENSTRUAL CYCLE by John Marshall](https://www.sciencedirect.com/science/article/pii/S0140673665915436)

# Period prediction
- there's always a drop in temp 1 or 2 days before period [source](https://invidious.kavin.rocks/watch?v=MZquDVwR78E)

