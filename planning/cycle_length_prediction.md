# Predict period cycle based on:

- existing cycle lengths
- temperature
- stress

## Existing cycle lengths

- probability is higher for most often occurring cycle length, lower for rarer lengths
- length in between usual lengths should be weighted as more likely than lengths among rare lengths

→ should be a probability curve

## Temperature

- **TODO:** find out how temperature can indicate that period is close and use as modifier for likelihood

## Stress

- stress could trigger the period before it's due
- **TODO:** research how big the effect of stress is and also use it to modify probability of menstruation on certain day 
