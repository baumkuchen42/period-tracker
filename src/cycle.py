import pandas as pd
import numpy as np
from datetime import date, timedelta

from period import Period
from hyper_params import VARIATION, NR_POSSIBLE_BEGINNINGS, DEFAULT_MAX_CYCLE_LENGTH
from utils import get_rounded_current_datetime, add_cycle_units_to_datetime
from exceptions import TooLongCycleException

class Cycle():
	attributes = [
		'cycle_unit',
		'date',
		'bleeding',
		'temperature',
		'menstruation_probability',
		'fertility_probability',
		'symptoms'
	]
	cycle_lengths = []
	bleeding_lengths = []
	current_cycle: Period
	units = []
	dates = []
	bleeding = []

	def __init__(self, periods: list[Period, ...]):
		self.add_ends_to_periods(periods)
		for period in periods:
			self.add_period_to_cycle(period)
			print(period)

		self.data = {
			'cycle_unit': self.units, 'date': self.dates, 'bleeding': self.bleeding,
			'menstruation_probability': [1 if b == True else 0 for b in self.bleeding]
		}
		self.dataframe = pd.DataFrame(self.data, columns=self.attributes)
		self.cycle_lengths_df = pd.DataFrame(self.cycle_lengths, columns=['Cycle length'])
		self.bleeding_lengths_df = pd.DataFrame(self.bleeding_lengths, columns=['Bleeding length'])

	def add_ends_to_periods(self, periods: list[Period, ...]):
		periods.sort(key=lambda p: p.start)
		max_cycle_len = self.calc_max_expected_cycle_length()
		for i in range(len(periods)):
			if i == len(periods) -1:
				break
			else:
				distance = periods[i+1].start - periods[i].start
				distance_in_units = distance.days * 4
				if distance_in_units > max_cycle_len:
					raise TooLongCycleException
				else:
					periods[i].end = add_cycle_units_to_datetime(periods[i+1].start, -1)

	def add_period_to_cycle(self, period):
			end = period.end or get_rounded_current_datetime()
			new_dates = pd.date_range(start=period.start, end=end, freq='6H')
			bleeding_dates = pd.date_range(start=period.start, end=period.bleeding_end, freq='6H')
			self.dates += new_dates
			cycle_len = len(new_dates)
			bleeding_len = len(bleeding_dates)
			self.units += [i for i in range(1, cycle_len+1)]

			if period.end is None:
				self.current_cycle = period
			else:
				self.cycle_lengths.append(cycle_len)
				self.bleeding_lengths.append(bleeding_len)

			self.bleeding += [True for i in range(bleeding_len)]
			self.bleeding += [False for i in range(cycle_len - bleeding_len)]

	def calc_menstruation_probability(self):
		probs = []

		until = self.calc_max_expected_cycle_length()
		units = [i for i in range(1, until+1)]
		distr = self.cycle_lengths_df.plot.density(ind=units).get_lines()[0].get_xydata()
		probs = distr[:, 1]

		return probs

	def calc_possible_period_beginnings(self):
		probs = self.calc_menstruation_probability()
		possible_units = np.argpartition(probs, -NR_POSSIBLE_BEGINNINGS)[-NR_POSSIBLE_BEGINNINGS:]
		possible_dates = []

		for i in possible_units:
			date = add_cycle_units_to_datetime(self.current_cycle.start, i)
			possible_dates.append(date)

		probs_of_possibles = [probs[i] for i in possible_units]

		return pd.DataFrame({'unit': possible_units, 'date': possible_dates, 'probability': probs_of_possibles})

	def calc_max_expected_cycle_length(self):
		if len(self.cycle_lengths) == 0:
			return DEFAULT_MAX_CYCLE_LENGTH
		else:
			return max(self.cycle_lengths) + VARIATION
