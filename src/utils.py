from datetime import datetime, date, timedelta

def get_rounded_current_datetime():
	now = datetime.now()
	today = date.today()
	night = datetime(today.year, today.month, today.day, 0)
	morning = datetime(today.year, today.month, today.day, 6)
	afternoon = datetime(today.year, today.month, today.day, 12)
	evening = datetime(today.year, today.month, today.day, 18)

	if night <= now < morning:
		now = night
	elif morning <= now < afternoon:
		now = morning
	elif afternoon <= now < evening:
		now = afternoon
	else:
		now = evening

	return now

def add_cycle_units_to_datetime(dt, unit):
	return dt + timedelta(hours= int(unit) * 6)
