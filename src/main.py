import matplotlib.pyplot as plt
import pandas as pd

from datetime import datetime

from cycle import Cycle
from period import Period

def plot(cycle):
	fig, axes = plt.subplots(2,1)

	cycle.bleeding_lengths_df.boxplot(ax=axes.flatten()[0], vert=False)
	cycle.cycle_lengths_df.boxplot(ax=axes.flatten()[1], vert=False)

	plt.tight_layout()
	plt.show()

if __name__=='__main__':
	cycle = Cycle([
		# Period(start=datetime(2020, 6, 4, 18), bleeding_len=1), # to test exception
		Period(start=datetime(2021, 7, 5, 12), bleeding_end=datetime(2021, 7, 9, 12)),
		Period(start=datetime(2021, 8, 2, 0), bleeding_end=datetime(2021, 8, 7, 12)),
		Period(start=datetime(2021, 8, 30, 0), bleeding_end=datetime(2021, 9, 5, 18))
	])
	print(cycle.dataframe)
	print('tracked cycle lengths:', cycle.cycle_lengths, 'current cycle:', cycle.current_cycle)
	print(cycle.calc_possible_period_beginnings())
	#plot(cycle)
