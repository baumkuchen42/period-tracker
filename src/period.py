from datetime import datetime

class Period():
	start: datetime = None
	bleeding_end: datetime = None
	end: datetime = None

	def __init__(self, **kwargs):
		self.__dict__.update(kwargs)

	def __str__(self):
		return f'<Period start: {self.start}, bleeding until: {self.bleeding_end}, end: {self.end}>'
