# Period Tracker

## Goal

The goal is to first write the logic in Python to accurately predict fertility and menstruation periods and then create a frontend for the application for Sailfish and for GNOME.

## Development status

So far, the logic for the initial entering of existing period data is finished. Next up is some planning and research of how to best predict cycle length. The goal is to have those predictions as percentages to provide a realistic image of when to expect your period. Planning and research material will be collected in the `planning` directory.

## How to run

You can test this application by executing the `main.py` script in the `src` folder of this project.
The dependencies for this project can be found in `requirements.txt`.
